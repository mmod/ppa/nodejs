# Node.js

This PPA is for auto-[re]packaging Node.js for Debian Testing (as needed) using the latest upstream package for Debian Stable [current]. I.E.: Debian Buster has a package dependency of python-minimal, which is python2-minimal on Debian Bullseye.

What this repository does is simply host a CI configuration that downloads the latest Node.js binary for Debian Stable, unpacks it and modifies the dependencies in it so that it will function for Debian Testing, repacks it and hosts it within a PPA.

The entire process is Automatic and driven by automated daily builds (initiated by a daily scheduled pipeline).

Auto DevOps at its finest!

## Using the PPA

To make use of this PPA:

#### Install the GPG Key

```bash
curl -s --compressed "https://mmod.gitlab.io/ppa/nodejs/key.gpg" | sudo apt-key add -
```

#### Add the List File

```bash
sudo curl -s --compressed -o /etc/apt/sources.list.d/mmod-ppa-nodejs.list "https://mmod.gitlab.io/ppa/nodejs/mmod-ppa-nodejs.list"
```

#### Update Apt

```bash
sudo apt-get update
```


#### Use the PPA

```bash
sudo apt-get install nodejs
```

## Conclusion

And that's it - the repository can be used as a template for any other type of PPA as well.